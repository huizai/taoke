<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\GetGoodsFromJidi::class,
        Commands\GoodsAutoOff::class,
        Commands\getQingtaoke::class,
        Commands\getGoodsByAlimama::class,
        Commands\GetMemuu::class,
        Commands\GetAlimamaOrder::class,
        Commands\SyncProductToEs::class,
        Commands\TaokeOrderBill::class,

        Sockets\pubServer::class,
        Sockets\subServer::class,
        Sockets\repServer::class,
        Sockets\reqServer::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
