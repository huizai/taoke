<?php

namespace App\Console\Sockets;
use Illuminate\Console\Command;

use DB;

class pubServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pub-server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '发布商品';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $addr = 'tcp://127.0.0.1:5555';
        $type = \ZMQ::SOCKET_PUB;
        $persistId = base64_encode($addr) . ':' . base64_encode(strval($type));
        $socket = new \ZMQSocket(new \ZMQContext(), $type, $persistId, function(\ZMQSocket $s) use($addr, $type) {
            \Log::info("ZeroMQ socket connecting to {$addr}");
            $s->connect($addr);
            if ($type === \ZMQ::SOCKET_PUB) {
                sleep(\Config::get('app.notice_sleep', 5));
            }
        });

        $socket->send("1 'aaaa'");
    }
}
