<?php

namespace App\Console\Sockets;
use Illuminate\Console\Command;

use DB;

class repServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rep-server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'rep server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $addr = 'tcp://127.0.0.1:5556';
        $type = \ZMQ::SOCKET_REP;

        $server = new \ZMQSocket(new \ZMQContext(1), $type);
        $server->bind($addr);
        while (true) {
            $request = $server->recv();
            printf ("Received request: [%s]\n", $request);
            $server->send("World");
        }
    }
}
