<?php

namespace App\Console\Sockets;
use Illuminate\Console\Command;

use DB;

class reqServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'req-server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'req server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $addr = 'tcp://127.0.0.1:5556';
        $type = \ZMQ::SOCKET_REQ;

        $server = new \ZMQSocket(new \ZMQContext(), $type);
        $server->connect($addr);

        $date = time();
        if($server->send($date) !== false){
            echo "send success\n";
        }
        $reply = $server->recv();
        printf ("Received:[%s]\n",$reply);
    }
}
