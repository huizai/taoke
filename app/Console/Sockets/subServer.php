<?php

namespace App\Console\Sockets;
use Illuminate\Console\Command;

use DB;

class subServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sub-server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '发布商品';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
            The server waits for messages from the client
            and echoes back the received message
        */
        $server = new \ZMQSocket(new \ZMQContext(), \ZMQ::SOCKET_SUB);
        $server->bind("tcp://127.0.0.1:5555");
        $server->setSockOpt(\ZMQ::SOCKOPT_SUBSCRIBE, 1);
        /* Loop receiving and echoing back */
        while ($message = $server->recv()) {
            $data = explode(' ', $message);
            if(!isset($data[1])){
                echo '数据错误';
                \Log::error('数据错误'.$message);
                return false;
            }

            $data = json_decode($data[1]);
            var_dump($data);
            /* echo back the message */
//            $server->send($message);
        }
    }
}
