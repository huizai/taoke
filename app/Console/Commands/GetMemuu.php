<?php

namespace App\Console\Commands;

use App\Libs\BLogger;
use App\Models\ConvertUrls;
use App\Models\Product;
use Illuminate\Console\Command;

use DB;

class GetMemuu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-memuu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '从美木获取商品';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $houre = date('H', time());
        if($houre >= 23 || $houre < 9){
            return;
        }

        $productModel = new Product();
        $url = 'http://www.memuu.com/api/v2/miaosha?search=&page=1&count=20&cid=0&from=mobileWeb&c_num=0&hidehb=0&token=null&appid=null';
        $goodsList = curlGet($url);
        $goodsList = json_decode($goodsList);
        $goodsCount = count($goodsList->goods);
        for ($i = $goodsCount-1; $i>=0; $i--){
            $tbid = $goodsList->goods[$i]->tbid;
            if($tbid != 558861124560) {
                $saveData = [
                    'class_id' => $goodsList->goods[$i]->cid,
                    'tbid' => $goodsList->goods[$i]->tbid,
                    'goods_name' => $goodsList->goods[$i]->title,
                    'taobao_title' => $goodsList->goods[$i]->otitle,
                    'goods_pic' => $goodsList->goods[$i]->imgurl,
                    'coupon_link' => $goodsList->goods[$i]->coupon_link_m,
                    'quan_fee' => $goodsList->goods[$i]->coupon_price * 100,
                    'guid_content' => $goodsList->goods[$i]->slogan,
                    'commission_rate' => $goodsList->goods[$i]->past_rate,
                    'taoke_price' => (int)$goodsList->goods[$i]->pprice * 100,
                    'sales_price' => (int)$goodsList->goods[$i]->tbprice * 100,
                    'tmall' => 0,
                    'sort' => 3,
                    'is_top' => 0,
                    'update_time' => date('Y-m-d H:i:s', time()),
                    'delete_time' => null
                ];
                DB::beginTransaction();
                try {
                    $data = DB::table($productModel->getTable())->where('tbid', $tbid)->first();
                    if ($data) {
                        if (DB::table($productModel->getTable())->where('tbid', $tbid)->update($saveData) === false) {
                            BLogger::getLogger('GET_GOODS')->info("美木---商品--{$tbid}--更新失败");
                            DB::rollBack();
                            continue;
                        } else {
                            if ($productModel->updateEsData($data->id, $saveData)) {
                                BLogger::getLogger('GET_GOODS')->info("商品--{$tbid}--更新到es数据成功");
                            } else {
                                BLogger::getLogger('GET_GOODS')->info("商品--{$tbid}--更新到es数据失败");
                            }
                        }
                    } else {
                        $saveData['create_time'] = date('Y-m-d H:i:s', time());
                        $id = DB::table($productModel->getTable())->insertGetId($saveData);
                        if (!$id) {
                            BLogger::getLogger('GET_GOODS')->info("美木---商品--{$tbid}--添加失败");
                            DB::rollBack();
                            continue;
                        } else {
                            if ($productModel->updateEsData($id, $saveData)) {
                                BLogger::getLogger('GET_GOODS')->info("商品--{$tbid}--更新到es数据成功");
                            } else {
                                BLogger::getLogger('GET_GOODS')->info("商品--{$tbid}--更新到es数据失败");
                            }
                        }
                    }

                    $convertUrlModel = new ConvertUrls();
                    if(DB::table($convertUrlModel->getTable())->where('tbid', $tbid)->delete() === false){
                        DB::rollBack();
                        BLogger::getLogger('GET_GOODS')->info("美木---已转的链接--{$tbid}--删除失败");
                        continue;
                    }
                    DB::commit();
                    BLogger::getLogger('GET_GOODS')->info("美木---商品--{$tbid}--更新/添加成功");
                } catch (\Exception $e) {
                    DB::rollBack();
                    BLogger::getLogger('GET_GOODS')->error($e);
                }
            }
        }
    }
}
