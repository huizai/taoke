<?php

namespace App\Console\Commands;
use App\Libs\Taobao\Taobao;
use Illuminate\Console\Command;

class getGoodsByAlimama extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-goods-from-ali';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '获取阿里妈妈商品';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $taobaoLib = new Taobao();
        $taobaoData = $taobaoLib->getTbGoodsList('面膜');
//        $taobaoData = $taobaoLib->getTbGoodsInfo($tbids);

        if(!isset($taobaoData->results->n_tbk_item) || empty($taobaoData->results->n_tbk_item)){
            return null;
        }
        foreach ($taobaoData->results->n_tbk_item as $tbkItem){
            dd($tbkItem);
            $couponData = $taobaoLib->getDgItemCoupon($tbkItem->title);
            dd($couponData);
        }
        dd($taobaoData);

        exit;
    }
}
