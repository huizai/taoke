<?php

namespace App\Console\Commands;

use App\Libs\BLogger;
use App\Libs\CouponInfo;
use App\Libs\Taobao\Taobao;
use App\Models\Product;
use Illuminate\Console\Command;

use DB;

class GoodsAutoOff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goods-auto-off';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '下架商品';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $page = 0;
        $length = 1000;

        do {
            $offset = $page * $length;
            $productModel = new Product();
            $products = DB::table($productModel->getTable())
                ->whereNull('delete_time')
                ->skip($offset)->take($length)
                ->get();
            foreach ($products as $product) {
                $updateData = [];
                $off = 0;
                $itemId = $product->tbid;
                $couponObj = new CouponInfo($itemId);
                $couponInfo = $couponObj->getCouponInfo();
                if(!$couponInfo['data']['pageList']){
                    $off = 1;
                    BLogger::getLogger('GOODS_AUTO_OFF')->info("商品{$itemId}优惠券过期,下架");
                }else {
                    $coupon = $couponInfo['data']['pageList'][0];
                    $updateData = [
                        'goods_name' => $product->goods_name,
                        'quan_fee' => $coupon['couponAmount'] * 100,
                        'sales' => $coupon['biz30day'],
                        'taobao_title' => $coupon['title'],
                        'tbid' => $coupon['auctionId'],
                        'commission_rate' => $coupon['tkRate'],
                        'tmall' => $product->tmall,
                        'sort' => $product->sort,
                        'is_top' => $product->is_top,
                        'taoke_price' => $coupon['zkPrice'] * 100 - $coupon['couponAmount'] * 100,
                        'sales_price' => $coupon['zkPrice'] * 100,
                        'update_time' => date('Y-m-d H:i:s', time())
                    ];
                    BLogger::getLogger('GOODS_AUTO_OFF')->info("下架脚本--更新商品{$itemId}优惠券");
                }

                if($off){
                    if(DB::table($productModel->getTable())->where('tbid',$itemId)->update([
                        'sort' => 0,
                        'delete_time' => date('Y-m-d H:i:s', time())
                    ]) !== false){
                        if($productModel->deleteEsData($product->id)){
                            BLogger::getLogger('GOODS_AUTO_OFF')->info("下架脚本--商品--{$product->tbid}--删除到es数据成功");
                        }else{
                            BLogger::getLogger('GOODS_AUTO_OFF')->info("下架脚本--商品--{$product->tbid}--删除到es数据失败");
                        }
                    }
                }else{
                    if(DB::table($productModel->getTable())->where('tbid',$itemId)->update($updateData) !== false){
                        if($productModel->updateEsData($product->id, $updateData)){
                            BLogger::getLogger('GOODS_AUTO_OFF')->info("下架脚本--商品--{$product->tbid}--更新到es数据成功");
                        }else{
                            BLogger::getLogger('GOODS_AUTO_OFF')->info("下架脚本--商品--{$product->tbid}--更新到es数据失败");
                        }
                    }
                }
                sleep(2);
            }
            $page++;
        }while(empty($products));
    }
}
