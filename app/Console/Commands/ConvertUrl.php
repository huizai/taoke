<?php

namespace App\Console\Commands;

use App\Libs\Taobao\Taobao;
use App\Models\ConvertUrls;
use App\Models\Product;
use Illuminate\Console\Command;

class ConvertUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert-url';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '从基地获取商品信息';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    }
}
