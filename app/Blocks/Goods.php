<?php

namespace App\Blocks;

use App\Libs\ConvertUrl;
use App\Models\Category;
use App\Models\ConvertUrls;
use App\Models\Product;

use DB;

class Goods
{
    public function getGoodsList($search=[], $offset=0, $length=20, $from='db'){
        $productModel = new Product();
        $convertUrlModel = new ConvertUrls();
        $categoryModel = new Category();

        $dataModel = DB::table($productModel->getTable().' as pm')
            ->select(
                'pm.*',
                'cum.url',
                'cum.taosec',
                'cm.category_name'
            )
            ->leftJoin($convertUrlModel->getTable().' as cum','cum.tbid', '=', 'pm.tbid')
            ->leftJoin($categoryModel->getTable().' as cm', 'cm.id', '=', 'pm.class_id')
            ->whereNull('delete_time');

        if(isset($search['id'])){
            $dataModel->where('pm.id', $search['id']);
        }
        if(isset($search['cid']) && !empty($search['cid']) && strtolower($search['cid']) != 'null'){
            $dataModel->where('pm.class_id', $search['cid']);
        }

        if(isset($search['tbids'])){
            $dataModel->whereIn('pm.tbid', $search['tbids']);
        }

        if(isset($search['tbid'])){
            $dataModel->where('pm.tbid', $search['tbid']);
        }

        $data = $dataModel
            ->orderBy('sort', 'desc')
            ->orderBy('update_time', 'desc')
            ->skip($offset)->take($length)
            ->get()->toArray();

        foreach ($data as $d){
            $d->quan_fee = del0(priceIntToFloat($d->quan_fee));
            $d->taoke_price = del0(priceIntToFloat($d->taoke_price));
            $d->sales_price = del0(priceIntToFloat($d->sales_price));

            preg_match('/_[\d]+x[\d]+/', $d->goods_pic, $match);
            if(empty($match)){
                $d->goods_pic = $d->goods_pic . '_430x430q90.jpg';
            }else {
                $d->goods_pic = preg_replace('/_[\d]+x[\d]+/', '_430x430', $d->goods_pic);
            }

            $d->from = $from;
        }
        return $data;
    }

    public function getGoodsInfo($goodsId){
        $data = $this->getGoodsList(['tbid'=>$goodsId],0,1);
        if(empty($data)){
            return null;
        }else{
            $data = $data[0];
            if(empty($data->url) || empty($data->taosec)){
//                try {
//                    $convertUrlLib = new ConvertUrl();
//                    $url = $convertUrlLib->convertUrl($data->tbid);
//                    if (isset($url['error'])) {
//                        return ['error' => $url['error']];
//                    }
//
//                    $data->coupon_invalid = $url['coupon_invalid'];
//
//                    if (!$data->coupon_invalid) {
//                        $convertUrlModel = new ConvertUrls();
//                        if (DB::table($convertUrlModel->getTable())->where('tbid', $data->tbid)->count()) {
//                            DB::table($convertUrlModel->getTable())->where('tbid', $data->tbid)->update([
//                                'url' => $url['url'],
//                                'taosec' => $url['taosec']
//                            ]);
//                        } else {
//                            DB::table($convertUrlModel->getTable())->insert([
//                                'tbid' => $data->tbid,
//                                'url' => $url['url'],
//                                'taosec' => $url['taosec']
//                            ]);
//                        }
//                    }
//
//                    $data->url = $url['url'];
//                    $data->taosec = $url['taosec'];
//                }catch (\Exception $exception){
//                    \Log::debug($exception);
//                }
                unset($data->url);
                unset($data->taosec);
            }
            return $data;
        }
    }
}
