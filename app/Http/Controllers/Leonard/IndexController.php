<?php

namespace App\Http\Controllers\Leonard;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;

class IndexController extends Controller {
    public function index(){
        return response()->json(ResponseMessage::getInstance()->success([])->response());
    }
}