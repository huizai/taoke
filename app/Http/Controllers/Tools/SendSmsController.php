<?php

namespace App\Http\Controllers\Tools;

use App\Models\SmsCsid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Config , Log , Validator , Cache;

use App\Libs\Sms;

class SendSmsController extends Controller
{

    public function checkCode(Request $request){
        $validation = Validator::make($request->all() , [
            'mobile'                  => 'required',
        ] , [
            'mobile.required'               => '没有填写手机号',
        ]);
        if($validation->fails()){
            $error = $validation->errors()->all();
            return response()->json(['error' => $error[0]]);
        }

        $mobile = str_replace(' ', '', trim($request->get('mobile')));

        if(!isMobileNum($mobile)){
            return response()->json(['error' => '不支持该号码']);
        }

        $code = getSalt('6' , 1);

        $time = 1;

        if($mobile == '18874130125'){
            $code = '123456';
        }
        Cache::put($mobile , $code , $time);

        Log::debug('验证码:'.$code);

        if($mobile != '18874130125') {
            $res = Sms::sendSMS([
                'm' => $mobile,
                'action' => 'sendParam',
                'c' => Config::get('sms.content.register'),
                'p1' => $code,
                'p2' => $time
            ]);
        }else{
            $res = true;
        }

        if($res === true) {
            return response()->json(['date' => '操作成功']);
        }else{
            return response()->json(['error' => $res]);
        }
    }

    public function thirdCheckCode(Request $request){
        $validation = Validator::make($request->all() , [
            'mobile'                    => 'required',
            'csid'                      => 'required',
            'code'                      => 'required',
            'sign'                      => 'required'
        ] , [
            'mobile.required'               => '没有填写手机号',
            'csid.required'                 => '没有填写csid',
            'code.required'                 => '验证码必须传',
            'sign.required'                 => 'sign必须传',
        ]);
        if($validation->fails()){
            $error = $validation->errors()->all();
            return response()->json(['error' => $error[0]]);
        }

        $mobile = $request->get('mobile');

        if(!isMobileNum($mobile)){
            return response()->json(['error' => '不支持该号码']);
        }

        $code = $request->get('code');
        $csid = $request->get('csid');

        $sign = $request->get('sign');

        $data = $request->all();
        unset($data['sign']);

        ksort($data);

        $smsCsidData = SmsCsid::where('csid' , $csid)->first();

        if(!$smsCsidData || $smsCsidData->num <= 0){
            return response()->json(['error' => 'csid is not exit']);
        }

        if(md5(implode('|' , $data) . '|' . $smsCsidData->secret) != $sign){
            return response()->json(['error' => '验证不通过']);
        }

        $time = 1;
        if($request->has('time')){
            $time = $request->get('time');
        }

        Log::debug('验证码:'.$code);

        $res = Sms::sendSMS([
            'm' => $mobile ,
            'action' => 'sendParam' ,
            'c' => Config::get('sms.content.register') ,
            'p1' => $code ,
            'p2' => $time ,
            'csid' => $csid
        ]);

        if($res === true) {
            SmsCsid::where('csid' , $csid)->update([
                'num' => $smsCsidData->num - 1
            ]);
            return response()->json(['data' => '请求成功']);
        }else{
            return response()->json(['error' => $res]);
        }

    }
}
