<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderCommission extends Model
{

    protected $table = 'order_commission';
    public $timestamps = false;
}
