<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PidOrder extends Model
{

    protected $table = 'pid_order';
    public $timestamps = false;
}
