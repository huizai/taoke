<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaokeWithdrawCash extends Model
{
    public $timestamps = false;
    protected $table = 'withdraw_cash_logs';
    protected $fillable = ['uid', 'amount', 'service_charge', 'remark', 'status', 'created_at'];
}
