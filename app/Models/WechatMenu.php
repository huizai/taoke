<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatMenu extends Model
{

    protected $table = 'wechat_menu';
    //guarded不可批量赋值的属性 //fillable可批量赋值的属性
    protected $guarded = [];
    public $timestamps = false;
}