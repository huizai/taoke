<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConvertUrls extends Model
{
    public $timestamps = false;
    protected $table = 'convert_urls';
}
