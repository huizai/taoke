<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class WechatUser extends Model
{

    protected $table = 'wechat_user';
    public $timestamps = false;

    public function saveData($data){
        return DB::table($this->table)->insertGetId($data);
    }

    public function getWeixinUser($openid){
        return DB::table($this->table)->where('openid', $openid)->first();
    }
}
