<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlimamaAcount extends Model
{

    protected $table = 'alimama_account';
    public $timestamps = false;
}
