<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlimamaOrder extends Model
{

    protected $table = 'taobao_order';
    public $timestamps = false;
}
