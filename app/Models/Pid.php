<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pid extends Model
{

    protected $table = 'alimama_pid';
    public $timestamps = false;
}
