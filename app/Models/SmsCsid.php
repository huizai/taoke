<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsCsid extends Model
{
    protected $table = 'sms_csid';
    protected $timestamp = false;

}
