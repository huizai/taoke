<?php

namespace App\Libs;

class CouponInfo
{
    private $activityId;
    private $itemId;
    private $url;
    private $couponData;

    public function __construct($itemId)
    {
        $this->itemId = $itemId;
        $this->url = "http://pub.alimama.com/items/search.json?q=https%3A%2F%2Fdetail.tmall.com%2Fitem.htm%3Fid%3D{$this->itemId}&_t=".getMillisecond()."&toPage=1&dpyhq=1&auctionTag=&perPageSize=50&shopTag=dpyhq&t=".getMillisecond()."&_tb_token_=ea17be8b6a46d";
        $this->couponData = json_decode(curlGet($this->url), true);
    }

    public function getCouponInfo(){
        return $this->couponData;
    }

    public function getCouponPrice(){
        \Log::debug("优惠券信息:".json_encode($this->couponData));
        dd($this->couponData);

    }
}