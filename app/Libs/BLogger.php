<?php
namespace App\Libs;

use Monolog\Logger;
use Illuminate\Log\Writer;

class BLogger
{
    // 所有的LOG都要求在这里注册
    const LOG_ERROR         = 'ERROR';
    const LOG_REQUEST       = 'REQUEST';
    const LOG_RESPONSE      = 'RESPONSE';
    const LOG_SQL           = 'SQL';
    const LOG_SMS           = 'SMS';
    const LOG_WECHAT_PAY    = 'WECHAT_PAY';
    const LOG_JPUSH         = 'JPUSH';
    const LOG_SCRIPT        = 'SCRIPT';
    const Log_ALIMAMA_ORDER = 'ALIMAMA_ORDER';
    const Log_GOODS_AUTO_OFF = 'GOODS_AUTO_OFF';
    const Log_GET_GOODS     = 'GET_GOODS';
    const LOG_ORDER_BILL    = 'ORDER_BILL';

    private static $loggers = array();

    // 记录日志
    public static function getLogger($type = self::LOG_ERROR, $day = 30)
    {
        self::$loggers[$type] = new Writer(new Logger($type));
        $log = self::$loggers[$type];
        $log->useDailyFiles(storage_path().'/logs/'. $type .'.log', $day );
        return $log;
    }
}