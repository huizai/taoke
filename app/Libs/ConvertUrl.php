<?php

namespace App\Libs;
use App\Libs\Taobao\Taobao;

class ConvertUrl {

    public function convertUrl($tbid, $couponUrl=null, $pid=null){

        $responseData = [];

        $jidi = new Jidi();
        $taobao = new Taobao();

        $accessToken = $jidi->getToken();
        $goodsInfos = $taobao->getTbGoodsInfo([$tbid]);

        if(!isset($goodsInfos->results->n_tbk_item) || empty($goodsInfos->results->n_tbk_item)){
            \Log::debug('获取商品失败--' . json_encode($tbid));
            return ['error' => 'get goods from taobao failed'];
        }

        $goodsInfo = $goodsInfos->results->n_tbk_item[0];
        $goodsName = $goodsInfo->title;

        $convertUrlData = [
            'token' => $accessToken,
            'taobao_id' => $tbid
        ];

        if($couponUrl){
            $convertUrlData['couponUrl'] = $couponUrl;
        }

        if($pid){
            $convertUrlData['pid'] = $pid;
        }

        $convertUrl = curlPost($jidi->getConvertUrl(), $convertUrlData);
        $convertUrl = json_decode($convertUrl);

        if(!$convertUrl || isset($convertUrl->error) || $convertUrl->status_code != 200){
            \Log::debug('转链接失败--' . json_encode($convertUrl));
            return ['error' => '转链接失败'];
        }
        $responseData['coupon_invalid'] = 0;
        if(!empty($couponUrl)) {
            $couponParam = parse_url($convertUrl->data->couponLike);
            if (!isset($couponParam['query'])) {
                \Log::debug('优惠券链接错误--' . json_encode($couponParam));
                return ['error' => 'coupon url error' . json_encode($couponParam)];
            }
            $couponInfoUrl = 'https://uland.taobao.com/cp/coupon?' . $couponParam['query'];
            $couponInfo = json_decode(curlGet($couponInfoUrl));

            if (
                isset($couponInfo->error)
                || !$couponInfo->success
                || empty($couponInfo->result->amount)
            ) {
                $responseData['coupon_invalid'] = 1;
            }

            $responseData['coupon_link'] = $couponUrl;
            $responseData['quan_fee'] = $couponInfo->result->amount;
            $responseData['coupon_end'] = strtotime($couponInfo->result->effectiveEndTime);
            $responseData['tmall'] = $couponInfo->result->item->tmall;
        }

        $url = $convertUrl->data->couponLike;

        $responseData['url'] = $url;
        $responseData['commission_rate'] = $convertUrl->data->info->max_commission_rate;
        $responseData['class_id'] = $convertUrl->data->info->category_id;

        $taosecData = $taobao->createTaosec($tbid, $goodsName, $convertUrl->data->couponLike, 'http://taoke.api.guoxiaoge.cn/imgs/quan.jpg');

        if (isset($taosecData->data->model)) {
            $taosec = $taosecData->data->model;
            $responseData['taosec'] = $taosec;
            \Log::info('tbid:' . $tbid . ' url:' . $url . ' taosec:' . $taosec);
            return $responseData;
        } else {
            \Log::debug('生成淘口令失败--' . json_encode($taosecData));
            return ['error' => '生成淘口令失败'];
        }
    }
}