<?php

namespace App\Libs\Taobao;

use Config;

require_once realpath(__DIR__) . '/taobao-sdk/TopSdk.php';

class Taobao {

    private $_taobaoSdk;
    private $_refreshToken;
    private $_tb_refresh_token_url;

    public function __construct($type = 1)
    {
        $this->_taobaoSdk               = new \TopClient();
        $this->_taobaoSdk->appkey = env('TAOBAO_APPKEY');
        $this->_taobaoSdk->secretKey = env('TAOBAO_SECRETKEY');
        $this->_taobaoSdk->format       = 'json';
        $this->_tb_refresh_token_url    = env('TAOBAO_REFRESS_TOKEN_URL');
        $this->_refreshToken            = env('TAOBAO_REFRESS_TOKEN');
    }

    public function getTbGoodsInfo($tbids = array()){

        $result = false;

        if(is_array($tbids) && !empty($tbids)) {
            $tbids = implode(',', $tbids);

            $request = new \TbkItemInfoGetRequest();
            $request->setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,nick,seller_id,volume,cat_name");
            $request->setPlatform('1');
            $request->setNumIids($tbids);
            $result = $this->_taobaoSdk->execute($request);
        }

        return $result;
    }

    public function getTbGoodsList($q=null, $cat_ids=null, $sort=null, $isTmall=false, $isOverseas=false, $itemloc=null, $pageNo=1, $pageSize=40) {
        if(empty($q) && empty($cat_ids)){
            return null;
        }
        $req = new \TbkItemGetRequest();
        $req->setFields("num_iid,title,pict_url,zk_final_price,item_url,seller_id,volume,nick,cate_id");
        if(!empty($q)) {
            $req->setQ($q);
        }
        if(!empty($cat_ids)) {
            $req->setCat(implode(",", $cat_ids));
        }
        if(!empty($itemloc)) {
            $req->setItemloc($itemloc);
        }
        if(!empty($sort)) {
            $req->setSort("tk_total_sales_des");
        }
        $req->setIsTmall($isTmall);
        $req->setIsOverseas($isOverseas);
        $req->setPlatform("1");
        $req->setPageNo($pageNo);
        $req->setPageSize($pageSize);
        return $this->_taobaoSdk->execute($req);
    }

    public function createTaosec($userId, $text, $url, $logo){
        $req = new \TbkTpwdCreateRequest;
        $req->setUserId($userId);
        $req->setText($text);
        $req->setUrl($url);
        $req->setLogo($logo);
        $req->setExt("{}");
        return $this->_taobaoSdk->execute($req);
    }

    public function getCouponInfo($me){
        $req = new \TbkCouponGetRequest;
        $req->setMe($me);
        return $this->_taobaoSdk->execute($req);
    }

    public function getDgItemCoupon($q, $cat=null, $pageNo=1, $pageSize=20, $pid='150772906'){
        $req = new \TbkDgItemCouponGetRequest;
        $req->setPlatform("1");
        if(!empty($cat)) {
            $req->setCat($cat);
        }
        $req->setPageSize($pageSize);
        $req->setQ($q);
        $req->setPageNo($pageNo);
        $req->setAdzoneId($pid);
        return $this->_taobaoSdk->execute($req);
    }

    public function getSpread($url){
        $req = new \TbkSpreadGetRequest;
        $requests = new \TbkSpreadRequest;
        $requests->url=$url;
        $req->setRequests(json_encode($requests));
        return $this->_taobaoSdk->execute($req);
    }
}