<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Tools', 'middleware' => ['ao']], function (){
    Route::post('/send/check/code', 'SendSmsController@checkCode');
});

Route::group(['namespace'=>'Taoke', 'middleware' => ['ao']], function (){
    Route::get('jidi/token', 'GoodsController@getJidiToken');
    Route::get('token', 'GoodsController@token');
    Route::get('jidi/products', 'GoodsController@getProducts');

    Route::get('convert/url', 'GoodsController@getConvertUrl');
    Route::get('goods/list', 'GoodsController@getGoodsList');
    Route::get('goods/{tbid}', 'GoodsController@getGoodsInfo');

    Route::get('category', 'GoodsController@getCategoryList');

    Route::get('share/{to}', 'GoodsController@share');

    Route::get('search', 'GoodsController@search');
    Route::get('test', 'GoodsController@test');
    Route::get('test/es/data', 'GoodsController@testESData');
});

Route::group(['namespace'=>'Users', 'prefix' => 'wechat', 'middleware' => ['wechat.config','ao']], function (){
    Route::get('/callback-login', 'UserController@weixinCallback');
});

Route::group(['namespace'=>'Users', 'middleware' => ['auth','ao']], function (){
    Route::get('/user', 'TaokeAgentController@getUserInfo');
    Route::get('/father/user', 'TaokeAgentController@fatherUser');
    Route::get('/user/order', 'TaokeAgentController@getOrder');
    Route::post('/agent/apply', 'TaokeAgentController@agentApply');
    Route::post('/withdraw', 'TaokeAgentController@withdraw');
});

Route::group(['namespace'=>'Wechat', 'prefix' => 'wechat', 'middleware' => ['wechat.config','ao']], function (){
    Route::any('/server', 'ServerController@server');
    Route::get('/menu/set', 'ServerController@setMenu');
    Route::get('/js/sdk', 'ServerController@getJsSdk');
    Route::get('/login', 'ServerController@login');
});
