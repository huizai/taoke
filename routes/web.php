<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group([ 'prefix' => 'tool' , 'namespace' => 'Tools', 'middleware' => ['ao']] , function (){
    Route::get('add/evaluate', 'CouponController@addEvaluateView');
    Route::get('coupon/view/{key}', 'CouponController@couponView');
    Route::post('coupon/img', 'CouponController@couponImg');

    Route::get('pub/test', 'PubGoodsController@pubTest');

    Route::get('pub/goods', 'PubGoodsController@pubView');
    Route::post('pub/goods', 'PubGoodsController@pubGoods');
    Route::get('pub/goods2', 'PubGoodsController@pub2View');
    Route::post('pub/goods/from/qq', 'PubGoodsController@pubGoodsWithQQ');

    Route::get('goods/ext/img/view/{key}', 'CouponController@goodsExtImgView');
    Route::get('goods/ext/img2/view/{goodsId}', 'CouponController@goodsExtImg2View');
    Route::get('goods/ext/img2/{goodsId}', 'CouponController@goodsExtImg2');
});
