<?php

Route::group(['namespace'=>'Leonard', 'middleware' => ['ao']], function () {
    Route::any('/', 'IndexController@index');
});