<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_order_address
        (
          o_id int(10) unsigned not null
            primary key,
          name varchar(20) not null comment '收货人',
          mobile varchar(20) not null comment '收货电话',
          sex varchar(20) default '1' null comment '性别 1女 2男',
          province varchar(16) not null comment '省',
          city varchar(16) not null comment '市',
          district varchar(16) not null comment '区',
          address varchar(200) not null comment '详细地址',
          house_number varchar(200) not null comment '门牌号',
          constraint taoke_order_address_ibfk_1
          foreign key (o_id) references taoke_orders (id)
            on update cascade on delete cascade
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
