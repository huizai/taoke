<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoupons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_coupons
        (
          id int(10) unsigned not null auto_increment
            primary key,
          s_id int default '0' null,
          type int default '1' null comment '券类型',
          content varchar(200) null comment '文字内容',
          value int not null comment '券的实际价值根据不同的券类型计算',
          prerequisite int default '0' not null comment '使用券的条件;0为没有条件',
          effective_days int default '0' not null comment '有效的天数',
          end_at timestamp null default null comment '如果这个没有值,以effective_days为准',
          num int default '0' null comment '券发放的总数量',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          deleted_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
