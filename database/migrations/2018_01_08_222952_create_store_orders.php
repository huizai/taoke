<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_store_orders
        (
          id int(10) unsigned not null auto_increment
            primary key,
          o_id int(10) unsigned not null,
          s_id int(10) unsigned not null,
          coupon_id int default '0' null,
          coupon_value int default '0' null,
          s_order_num varchar(32) not null comment '订单号',
          service_charge int default '0' null comment '服务费、配送费',
          g_total_price int not null comment '商品总价',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
