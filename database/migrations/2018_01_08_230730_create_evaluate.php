<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_goods_evaluate
        (
          id int(10) unsigned not null auto_increment
            primary key,
          o_id int(10) unsigned not null,
          g_id int(10) unsigned not null,
          star int(10) default '0' null comment '星星数',
          tag varchar(255) default '' null comment '标签',
          evaluate text null comment '评价内容',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          imgs text null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
