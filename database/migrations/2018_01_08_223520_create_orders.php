<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_orders
        (
          id int(10) unsigned not null auto_increment
            primary key,
          u_id int null,
          order_num varchar(32) not null comment '订单号',
          service_charge int default '0' null comment '服务费、配送费',
          g_total_price int not null comment '商品总价',
          pay_total int not null comment '总共需支付的总价',
          coupon_value int default '0' null,
          status int default '1' null comment '订单状态',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          pay_time timestamp null default null
        )
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
