<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWxPay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_wx_pay
        (
          out_trade_no varchar(255) not null
            primary key,
          openid varchar(200) default '' null comment '微信OPENID',
          trade_type varchar(20) not null,
          oid int(10) unsigned not null,
          fee_type int not null,
          body varchar(255) not null,
          detail varchar(255) not null,
          total_fee int(10) unsigned not null,
          attach varchar(255) null,
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null,
          transaction_id varchar(200) default '' null comment '微信订单ID',
          refund_no varchar(255) null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
