<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreGoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_store_goods
        (
          id int(10) unsigned not null auto_increment
            primary key,
          s_id int(10) unsigned not null comment '店铺ID',
          c_id int(10) unsigned not null comment '分类ID',
          b_id int(10) unsigned default '0' null comment '品牌ID',
          show_title varchar(200) not null comment '显示的标题',
          name varchar(200) not null comment '商品名称',
          producing_area varchar(200) not null comment '产地',
          unit varchar(50) not null comment '规矩,一箱(6个装)!一件!或是一个',
          characteristic varchar(255) null comment '特点',
          stock_num int default '0' null,
          illustrate text null,
          now_price int not null comment '现价',
          out_price int not null comment '售价',
          in_price int default '0' null comment '进价',
          status tinyint(1) default '0' null comment '1:上架 , 0:下架',
          on_off_time timestamp null default null comment '上架或下架的时间',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null,
          nav_id int(10) default '0' null,
          sort double default '0' null,
          constraint taoke_goods_s_id_store_id
          foreign key (s_id) references taoke_stores (id)
            on update cascade
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index taoke_goods_s_id_store_id
          on taoke_store_goods (s_id)
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
