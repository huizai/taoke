<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_carts
        (
          id int(10) unsigned not null auto_increment
            primary key,
          u_id int(10) unsigned not null comment '用户ID',
          g_id int(10) unsigned not null comment '商品ID',
          now_price int not null comment '现价',
          buy_num int default '0' not null comment '购买数量',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          s_id int(10) default '0' not null,
          constraint taoke_carts_ibfk_2
          foreign key (u_id) references taoke_users (id)
            on update cascade
        );
        
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index u_id on taoke_carts (u_id);
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
