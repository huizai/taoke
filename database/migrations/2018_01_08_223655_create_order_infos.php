<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInfos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_order_infos
        (
          id int(10) unsigned not null auto_increment
            primary key,
          o_id int(10) unsigned not null comment '订单ID',
          g_id int(10) unsigned not null comment '商品ID',
          c_id int(10) unsigned not null comment '分类ID',
          b_id int(10) unsigned default '0' null comment '品牌ID',
          b_name varchar(200) null comment '品牌名称',
          show_title varchar(200) not null comment '显示的标题',
          name varchar(200) not null comment '商品名称',
          producing_area varchar(200) not null comment '产地',
          unit varchar(50) not null comment '规矩,一箱(6个装)!一件!或是一个',
          now_price int not null comment '现价',
          buy_num int default '0' not null comment '购买数量',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null,
          s_id int not null,
          constraint taoke_order_infos_ibfk_3
          foreign key (o_id) references taoke_orders (id)
            on update cascade
        )
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index o_id
          on taoke_order_infos (o_id)
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
