<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_stores
        (
          id int(10) unsigned not null auto_increment
            primary key,
          name varchar(200) not null comment '店铺名称',
          address varchar(200) not null comment '联系地址',
          mobile varchar(20) not null comment '联系电话',
          contact varchar(50) not null comment '联系人',
          i_d_card varchar(255) not null comment '身份证',
          business_license varchar(255) not null comment '营业执照',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null,
          amap_id int default '0' null,
          password varchar(40) not null,
          salt varchar(16) not null,
          lng varchar(255) not null,
          lat varchar(255) not null,
          adcode varchar(6) not null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
