<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminNodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_admin_nodes
        (
          id int(10) unsigned not null auto_increment
            primary key,
          name varchar(200) not null,
          url varchar(200) not null,
          method varchar(200) not null,
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null,
          sort int default '1' null comment '排序'
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
