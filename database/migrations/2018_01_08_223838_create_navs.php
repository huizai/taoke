<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_navs
        (
          id int(10) unsigned not null auto_increment
            primary key,
          s_id int(10) unsigned not null comment '店铺ID',
          name varchar(200) not null,
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null,
          sort int default '0' null,
          constraint taoke_navs_s_id_store_id
          foreign key (s_id) references taoke_stores (id)
            on update cascade
        );
        
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index taoke_navs_s_id_store_id on taoke_navs (s_id);
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
