<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_products
        (
            id int(10) unsigned not null auto_increment
                primary key,
            class_id int(10) unsigned not null comment '分类id',
            goods_name varchar(255) not null comment '商品名称',
            goods_pic varchar(255) not null comment '商品图片',
            tbid varchar(15) not null comment '淘宝商品ID',
            coupon_link varchar(255) null comment '优惠券链接',
            quan_fee int default '0' null comment '优惠券价格;分为单位',
            taoke_price int default '0' null comment '领券后的价格;分为单位',
            sales_price int default '0' null comment '淘宝售价',
            guid_content varchar(255) null comment '导购文案',
            sales int default '0' null comment '销量',
            tmall smallint(6) default '1' null comment '是否天猫1，天猫，0 非天猫',
            commission_rate float default '0' null comment '佣金比例',
            me varchar(255) null comment '阿里妈妈券的me参数',
            create_time timestamp null,
            update_time timestamp default CURRENT_TIMESTAMP not null,
            coupon_received int default '0' null comment '优惠券已领数量',
            coupon_count int default '0' null comment '优惠券总数量',
            coupon_surplus int default '0' null comment '优惠券剩余数量',
            coupon_end int default '0' null comment '优惠券结束时间',
            commission_type smallint(6) default '2' null comment '佣金类型：1为定向，2为通用, 3为鹊桥，4为营销计划',
            is_merchants smallint(6) default '1' null comment '是否阿里妈妈券 1是阿里妈妈券，0非阿里妈妈券',
            delete_time timestamp null comment '下架时间',
            taobao_title varchar(255) null comment '淘宝标题',
            sort int default '0' null,
            is_top int default '0' null
        ) comment '商品表';
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index idx_delete_time
            on taoke_products (delete_time)
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index idx_tbid
            on taoke_products (tbid)
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
