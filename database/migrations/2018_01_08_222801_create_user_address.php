<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_user_address
        (
          id int(10) unsigned not null auto_increment
            primary key,
          u_id int(10) unsigned not null comment '用户ID',
          mobile varchar(20) not null comment '收货人电话',
          name varchar(20) not null comment '收货人姓名',
          sex varchar(20) default '1' null comment '性别 1女 2男',
          province varchar(16) not null comment '省',
          city varchar(16) not null comment '市',
          district varchar(200) default '' null,
          address varchar(200) not null comment '详细地址',
          house_number varchar(200) not null comment '门牌号',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null,
          lng varchar(255) not null,
          lat varchar(255) not null,
          useing tinyint default '0' null,
          constraint taoke_user_address_ibfk_1
          foreign key (u_id) references taoke_users (id)
            on update cascade on delete cascade
        )
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index u_id
          on taoke_user_address (u_id)
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
