<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsImgs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_goods_imgs
        (
          id int(10) unsigned not null auto_increment
            primary key,
          g_id int(10) unsigned not null,
          img_url varchar(255) not null comment '图片地址',
          created_at timestamp default CURRENT_TIMESTAMP not null,
          deleted_at timestamp null default null,
          sort int default '1' null comment '排序',
          constraint taoke_goods_imgs_ibfk_1
          foreign key (g_id) references taoke_goods (id)
            on update cascade on delete cascade
        );
        
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index g_id on taoke_goods_imgs (g_id);
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
