<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWechatConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_wechat_config
        (
            id int(10) unsigned not null auto_increment
                primary key,
            name varchar(200) not null comment '公众号名称',
            base_url varchar(255) null,
            type tinyint default '1' null comment '类型 1 订阅号 2 服务号',
            app_id varchar(255) not null comment '微信app_id',
            secret varchar(255) not null comment '微信秘钥',
            token varchar(255) null comment '微信token',
            aes_key varchar(255) null comment '微信aes_key',
            oauth_only_wechat_browser tinyint(1) default '0' null comment '只在微信浏览器跳转',
            oauth_scopes varchar(255) default 'snsapi_userinfo' null comment '公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login',
            oauth_callback varchar(255) null comment '登录回调链接',
            update_time timestamp default CURRENT_TIMESTAMP not null,
            constraint app_id
                unique (app_id)
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
