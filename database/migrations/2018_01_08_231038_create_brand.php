<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_brand
        (
          id int(10) unsigned not null auto_increment
            primary key,
          c_id int(10) unsigned not null comment '分类ID',
          name varchar(200) not null,
          created_at timestamp default CURRENT_TIMESTAMP not null,
          deleted_at timestamp null default null,
          constraint taoke_brand_ibfk_1
          foreign key (c_id) references taoke_categories (id)
            on update cascade
        );
        
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }

        $sql = <<<SQL
        create index c_id on taoke_brand (c_id);
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
