<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_admin_roles
        (
          id int(10) unsigned not null auto_increment
            primary key,
          permission varchar(255) default '' null comment '节点',
          name varchar(200) not null,
          description varchar(200) null,
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
