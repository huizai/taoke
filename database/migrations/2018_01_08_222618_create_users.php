<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_users
        (
          id int(10) unsigned not null auto_increment
            primary key,
          nick_name varchar(200) not null,
          mobile varchar(20) default '' null comment '手机号',
          email varchar(128) default '' null comment '邮箱',
          wx_openid varchar(200) default '' null comment '微信OPENID',
          headimgurl varchar(255) default '' null comment '头像',
          password varchar(32) default '' null,
          salt varchar(32) default '' null,
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null,
          deleted_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
