<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreNavs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_store_navs
        (
          id int(10) unsigned not null auto_increment
            primary key,
          s_id int(10) unsigned not null,
          name varchar(20) not null,
          sort int default '1' null,
          created_at timestamp default CURRENT_TIMESTAMP not null,
          updated_at timestamp null default null
        )
        ;
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
