<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmapCityCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_amap_city_code
        (
          adcode int(6) unsigned not null
            primary key,
          name varchar(45) not null,
          center varchar(45) not null,
          level varchar(45) not null,
          city_code varchar(6) null,
          constraint abcode_UNIQUE
          unique (adcode)
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
