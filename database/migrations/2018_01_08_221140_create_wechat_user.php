<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWechatUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        create table taoke_wechat_user
        (
            id int(10) unsigned not null auto_increment
                primary key,
            wechat_id int(10) unsigned not null comment '微信id',
            city varchar(200) null comment '城市',
            province varchar(200) null comment '省份',
            country varchar(200) null comment '国家',
            sex tinyint(1) default '0' null comment '性别',
            headimgurl text null comment '头像',
            subscribe tinyint default '1' null comment '是否关注;ps:有取消关注的',
            subscribe_time timestamp null default null comment '是否关注;ps:有取消关注的',
            create_time timestamp null,
            update_time timestamp default CURRENT_TIMESTAMP not null,
            openid varchar(40) not null comment '微信openid',
            nickname varchar(40) not null comment '微信昵称',
            constraint openid
                unique (openid)
        )comment '微信公众号与openid对应的关系';
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
