<?php

return [
    'index' => env('ELASTICSEARCH_INDEX', 'taoke'),
    'hosts' => [
        env('ELASTICSEARCH_HOST', 'http://122.114.222.155:9200'),
    ],
];
