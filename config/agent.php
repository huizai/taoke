<?php

return array(
    '1' => [
        'name' => '普通代理',
        'commission1' => 0.3 * 0.94,
        'commission2' => 0.2 * 0.94,
        'upgrade' => [
            'bill_order_num' => 0,
            'agent_num' => 0
        ]
    ],
    '2' => [
        'name' => '白银代理',
        'commission1' => 0.4 * 0.94,
        'commission2' => 0.25 * 0.94,
        'upgrade' => [
            'bill_order_num' => 10,
            'agent_num' => 5
        ]
    ],
    '3' => [
        'name' => '黄金代理',
        'commission1' => 0.5 * 0.94,
        'commission2' => 0.3 * 0.94,
        'upgrade' => [
            'bill_order_num' => 39,
            'agent_num' => 15
        ]
    ],
);
