<?php

return [
    'url'   => 'http://smsapi.c123.cn:80/OpenPlatform/OpenApi',
    'data'  => [
        'action'    => 'sendOnce',                                //发送类型 ，可以有sendOnce短信发送，sendBatch一对一发送，sendParam	动态参数短信接口
        'ac'        => '1001@501108780001',					                         //用户账号
        'authkey'   => '2AC48D756D3CA84AD484EB4ABC8B0756',	                             //认证密钥
        'cgid'      => '4843',                                       //通道组编号
        'm'         => '',		                                     //号码,多个号码用逗号隔开
        'c'         => '',		                                   //内容 utf8
        'csid'      => '8206',                                       //签名编号 ，可以为空，为空时使用系统默认的签名编号
        't'         => ''
    ],

    'content'   => [
        'register' => "验证码{p1},有效期{p2}分钟。正在进行手机绑定，请勿将验证码泄露于他人。",
        'pingan' => '中国平安售后部因发展需要多个岗位急聘，年薪{p1}，并提供公平透明的晋升空间。李经理{p2}'
    ],

];