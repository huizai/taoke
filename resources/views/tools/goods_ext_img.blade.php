<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=3.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>微信群发-商品图片</title>
    <link rel="stylesheet" href="{{ URL::asset('/') }}css/goods_ext_img2.css?v=2">
</head>
<body>
<div class="content">
    <div class="content-right">
        <div>{{ $title }}</div>
    </div>
    <div class="content-left">
        <div style="width: 100%;height: 350px;">
            <div class="goods">
                <div class="goods-img"><img src="{{ $goods_img }}" alt=""></div>
                <div class="goods-img-price">原价{{ $goods_price }}元</div>
            </div>
            <div style="margin-left: 74px;float: left">
                @if(!empty($coupon_price))
                    <div class="coupon-price">
                        <div class="coupon-icon">券</div>
                        <div class="coupon-value">{{ $coupon_price }}元</div>
                    </div>
                @endif
                <div class="coupon-qrcode" @if(empty($coupon_price)) style="margin-top: 55px" @endif>
                    <div class="qrcode">
                        <img src="{{ $qrcode }}" alt="">
                    </div>
                    <div class="qrcode-notice">长按识别二维码，即可领券</div>
                </div>
            </div>
        </div>
        <div class="coupon-after">
            @if(empty($coupon_price))
                秒杀价{{ $goods_price - $coupon_price }}元
            @else
                券后{{ $goods_price - $coupon_price }}元
            @endif

        </div>

        <div class="slogan">
            {{ $slogan }}
        </div>
    </div>
</div>
</body>
</html>