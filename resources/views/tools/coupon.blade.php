<!DOCTYPE html>
<html class="anmi" style="font-size: 47px;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" async="" src="{{ URL::asset('/') }}tool/coupon/entry.js"></script>
    <script type="text/javascript" async="" src="{{ URL::asset('/') }}tool/coupon/index.js" id="aplus-sufei"></script>
    <script src="{{ URL::asset('/') }}tool/coupon/0.js"></script>

    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="wap-font-scale" content="no">
    <meta id="WV.Meta.EnableLongPressEvent" value="true">
    <link rel="shortcut icon" href="https://img.alicdn.com/tps/TB1PlWbKFXXXXbmXFXXXXXXXXXX-16-16.ico"
          type="image/x-icon">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="data-spm" content="a311n">
    <meta name="aplus-touch" content="1">
    <meta name="aplus-terminal" content="1">
    <meta name="aplus-waiting" content="MAN">
    <title> </title>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/') }}tool/coupon/neat-min.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/') }}tool/coupon/index.css">
    <style>.list-item .coupon-amount-icon .icon-left {
            height: 1rem;
        }</style>
    <script src="{{ URL::asset('/') }}tool/coupon/saved_resource" crossorigin=""></script>
    <style id="mx_0">.atom-dialog-hide body {
            position: fixed;
            width: 100%
        }

        .atom-dialog-wrap {
            display: none;
            position: fixed;
            overflow: auto;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            -webkit-overflow-scrolling: touch;
            outline: 0
        }

        .atom-dialog-wrap .atom-dialog {
            position: relative;
            background: #fff;
            border-radius: 5px
        }

        .atom-dialog-wrap .atom-dialog .atom-dialog-title {
            text-align: center;
            font-size: 20px;
            padding: 10px
        }

        .atom-dialog-wrap .atom-dialog .atom-dialog-content {
            padding: 10px 20px
        }

        .atom-dialog-wrap .atom-dialog .atom-dialog-close {
            position: absolute;
            right: 3%;
            top: 5%;
            color: #333;
            height: 20px;
            line-height: 20px
        }

        .atom-dialog-wrap .atom-dialog .atom-dialog-close:after {
            content: "\D7";
            font-weight: 100;
            font-size: 20px
        }

        .atom-dialog-show {
            display: flex;
            display: -ms-flexbox;
            display: -webkit-box;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center
        }

        .atom-dialog-bottom-close {
            position: absolute;
            bottom: -2.5rem;
            left: 5.2rem;
            width: 1.7rem;
            height: 1.7rem;
            background: url(//gw.alicdn.com/tps/TB1nv7dNFXXXXaNXXXXXXXXXXXX-76-76.png);
            background-size: 1.7rem
        }
    </style>
    <script src="https://local.alipcsec.com:6691/umid/getumid?data=&amp;_callback=_8514_1511427307571"></script>
    <link rel="stylesheet" href="{{ URL::asset('/') }}tool/coupon/layer.css" id="layuicss-skinlayercss">
</head>
<body data-spm="8189758/a">
<div id="J_root">
    <div>
        <img style="
            position: absolute;
            z-index: 10;
        " src="{{ URL::asset('/') }}tool/coupon/shuiying.png" />
    </div>
    <div class="body-wrap">
        <div class="shop-info"><span class="shop-logo"><img class="logo"
                                                            src="{{ $shop_logo }}"></span><span
                    class="shop-title">{{ $shop_name }}</span>
            <div mx-view="app/views/ticket/coupons/index" data-spm="2192099" id="mx_6">
                <div class="coupons-wrap noFollow">
                    <div class="coupons-container js-can js-ready" mx-click="getCoupons()"><span
                                class="coupons-price"><strong>¥</strong>{{ $coupon_price }}</span><span class="coupons-info">使用期限</span>
                        <span class="coupons-data"><span class="coupons-data-icon">限</span>{{ $coupon_start }}-{{ $coupon_end }}</span>
                        <span class="coupons-btn">立即抢券</span></div>
                </div>
            </div>
        </div>
        <div mx-view="app/views/ticket/item/index" class="item-detail-view " data-spm="90200000" id="mx_4" style="background: #fff">
            <div class="item-wrap"><a
                        href=""
                        class="item-detail">
                    <div class="pic"><img
                                src="{{ $goods_img }}"></div>
                    <div class="item-content">
                        <div class="title"><span>{{ $goods_name }}</span></div>
                        <div class="tags"><span class="tmallTag tag"><img
                                        src="{{ URL::asset('/') }}tool/coupon/TB10U2vKFXXXXa3XXXXXXXXXXXX-36-36.png" alt=""> </span> <span
                                    class="byTag tag">包邮</span><span class="dealNum">{{ $sales }}笔成交</span></div>
                        <div class="price-origin"><span class="origin">现价：¥{{ $price }}</span></div>
                        <div class="discount"><img class="img-tag"
                                                   src="{{ URL::asset('/') }}tool/coupon/quanhou.png"> <span
                                    class="sale"><em>¥</em>{{ $end_price }}</span></div>
                    </div>
                </a><a href="https://s.click.taobao.com/t?e=m%3D2%26s%3DFeD41SdP8WBw4vFB6t2Z2ueEDrYVVa64K7Vc7tFgwiHjf2vlNIV67gvspZaHHOxqu6Vvho8Zh8APx7%2BmY1FbeNjxqBwllfQEe91xqutTdSs3Ek1XOgoh3NDm2E92EA5Uw%2FTEAIZ1KIsSIz%2BVIb9tlPNaMJZiEfvzcwc0XuCRcO%2F1c9qXLriRR9QrWf%2FPPSnCpNwHiHl2aNos0zmQwXmdXIrum%2FXk0e%2B6&amp;pvid=21291868"
                       class="link">查看
                    <svg class="link-arrow" viewBox="0 0 200 200">
                        <g transform="scale(0.1953125, 0.1953125)">
                            <path d="M282.7264 1004.714667c8.738133 0 17.476267-3.345067 24.132267-10.001067l458.615467-458.5472c13.346133-13.346133 13.346133-34.9184 0-48.264533L306.858667 29.2864c-13.346133-13.346133-34.9184-13.346133-48.264533 0s-13.346133 34.9184 0 48.264533L693.077333 512 258.594133 946.449067c-13.346133 13.346133-13.346133 34.9184 0 48.264533C265.250133 1001.3696 273.988267 1004.714667 282.7264 1004.714667z"
                                  fill="#272636"></path>
                        </g>
                    </svg>
                </a></div>
        </div>
        <div class="mid-banner" data-spm="hd"></div>
        <div class="activity-rules">
            <div class="activity-title"><span class="line"></span> <span class="title">活动说明</span></div>
            <span class="rule-line">1. 点击“立即抢券”按钮，抢券成功后购买商品时使用，即可享受优惠。</span> <span class="rule-line">2. 在手机淘宝-我的淘宝-卡券包-网店优惠券，可查询已发放到账户的优惠券。</span>
            <span class="rule-line">3. 因商品参与其它活动等原因，付款时优惠券可能无法使用，此时商品最终成交价以您实际付款时提示金额为准。</span> <span class="rule-line">4. 获取、使用优惠券时如存在违规行为（作弊领取、恶意套现、刷取信誉、虚假交易等），将取消用户领取资格、撤销违规交易且收回全部优惠券（含已使用及未使用的），必要时追究法律责任。</span>
            <span class="rule-line">5. 预售商品交付定金时，不可使用优惠券。 </span><span class="rule-line">6. 请仔细核对优惠券使用期，如双十一当天不在优惠券使用期，则该优惠券在支付尾款时不能使用。券后价以您当天实际支付金额为准。</span>
        </div>
        <div class="activity-rules">
            <div class="activity-title"><span class="line"></span> <span class="title">规则声明</span></div>
            <span class="rule-line">上传至该页面的商品与优惠券信息，会被阿里集团官方收录并给其他用户使用。上传者上传此类信息的，表明上传者已与商家达成一致，即商家同意官方收录且给其他用户使用。如商家不同意，请上传者不要上传，如否导致的纠纷由上传者解决并承担责任。</span>
        </div>
    </div>
</div>
</body>
</html>