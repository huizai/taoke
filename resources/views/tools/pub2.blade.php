<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <title>Document</title>


    <style>
        .pub-input-box{
            width: 500px;
            margin: 0 auto;
        }

        .pub-button{
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 90px;
        }
    </style>
</head>
<body>
<div class="pub-input-box"><textarea rows="10" cols="60" class="pub-input"></textarea></div>
<div class="pub-button"><button id="pub">发布</button></div>
</body>

<script>

    $('#pub').bind('click', function () {

        var data = $('.pub-input').val();
        $.ajax({
            url     : '/tool/pub/goods/from/qq',
            type    : 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : {data:data} ,
            dataType : 'json' ,
            success : function (data) {
                if(data.error == undefined){
                    window.location.href = '/tool/goods/ext/img2/'+ data.id;
                }else{
                    alert(data.error);
                }
            },
            error : function (error) {

            }
        });
    });

</script>
</html>