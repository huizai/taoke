var system = require('system');

url =  system.args[1];
var page = require('webpage').create();

page.settings.userAgent = 'mozilla/5.0 (iphone; cpu iphone os 5_1_1 like mac os x) applewebkit/534.46 (khtml, like gecko) mobile/9b206 micromessenger/5.0';
page.viewportSize = {
    width: 357,
    height: 540
};

page.open(url, function () {
    page.clipRect={
        top:0,
        left:0,
        width:704,
        height:1060
    };
    page.zoomFactor=1;
    window.setTimeout(function () {
        console.log(page.renderBase64('JPEG'));
        phantom.exit();
    } , 500);
});
